package com.myapp.myapplication

import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.skydoves.powerspinner.IconSpinnerAdapter
import com.skydoves.powerspinner.IconSpinnerItem
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : LocalizationActivity() {

    private val ID_ENGLISH = 0
    private val ID_FRENCH = 1
    private val ID_KOREAN = 2
    private val ID_GERMAN = 3
    private val ID_SPANISH = 4
    private val ID_CHINESE = 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinnerView.apply {
            setSpinnerAdapter(IconSpinnerAdapter(this))
            setItems(
                arrayListOf(
                    IconSpinnerItem(getDrawable(R.drawable.unitedstates), "English"),
                    IconSpinnerItem(getDrawable(R.drawable.france), "French"),
                    //IconSpinnerItem(getDrawable(R.drawable.canada), "Canada"),
                    IconSpinnerItem(getDrawable(R.drawable.southkorea), "Korean"),
                    IconSpinnerItem(getDrawable(R.drawable.germany), "German"),
                    IconSpinnerItem(getDrawable(R.drawable.spain), "Spanish"),
                    IconSpinnerItem(getDrawable(R.drawable.china), "Italian")
                )
            )

            getSpinnerRecyclerView().layoutManager = GridLayoutManager(baseContext, 2)
            preferenceName = "Language"
            lifecycleOwner = this@MainActivity
        }

        spinnerView.setOnSpinnerItemSelectedListener<IconSpinnerItem> { index, item ->
            when (index) {
                ID_ENGLISH -> {
                    setLanguage(Locale.ENGLISH)
                    return@setOnSpinnerItemSelectedListener
                }
                ID_FRENCH -> {
                    setLanguage(Locale.FRENCH)
                    return@setOnSpinnerItemSelectedListener

                }
                ID_KOREAN -> {
                    setLanguage(Locale.KOREAN)
                    return@setOnSpinnerItemSelectedListener

                }
                ID_GERMAN -> {
                    setLanguage(Locale.GERMAN)
                    return@setOnSpinnerItemSelectedListener

                }

                ID_SPANISH -> {
                    setLanguage("es")
                    return@setOnSpinnerItemSelectedListener

                }

                ID_CHINESE -> {
                    setLanguage(Locale.ITALIAN)
                    return@setOnSpinnerItemSelectedListener

                }
            }
        }
    }
}